package Ej2;

public class Pokemons implements Runnable{

        private int id;
        Thread t;
        public Pokemons(int id, Thread t) {
            this.id = id;
            this.t = t;
        }

        @Override
        public void run() {
            for(int i = 1; i <= id; i++) {
                if(i%2 != 0){
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("Soy el pokémon número " + i + "\n");
                }
            }
        }

}
