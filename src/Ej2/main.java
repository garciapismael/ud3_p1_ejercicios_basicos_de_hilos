package Ej2;

import java.util.Scanner;

public class main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Dame un número par: ");
        int id = sc.nextInt();

        if(id%2==0) {

            Thread trainer = new Thread(new Trainer(id));
            Thread pokemons = new Thread(new Pokemons(id, trainer));
            trainer.start();
            pokemons.start();
            try {
                pokemons.join();
                trainer.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }else{
            System.out.println("Error: Numero impar");
        }
    }
}

