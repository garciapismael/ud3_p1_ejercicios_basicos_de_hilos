package Ej2;

public class Trainer implements Runnable{
    private  int id;

    public Trainer(int id) {
        this.id = id;
    }

    @Override
    public void run() {
        for(int i = 1; i <= id; i++) {
            if(i%2 == 0){
                System.out.println("Soy el entrenador número " + i);
                try {
                    Thread.sleep(110);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
