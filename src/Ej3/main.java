package Ej3;

public class main {
    public static void main(String[] args) {
        Thread[] caballos = new Thread[10];
        //TODO: Utilizo las prioridades para hacer que le caballo sea mas lento o mas rapido
        for(int i = 0; i < 10; i++){
            if(i == 0){
                caballos[i] = new Thread(new ThreadHorse(i+1));
                caballos[i].setPriority(1);
                caballos[i].start();
            }else if(i == 9){
                caballos[i] = new Thread(new ThreadHorse(i+1));
                caballos[i].setPriority(10);
                caballos[i].start();
            }else{
                caballos[i] = new Thread(new ThreadHorse(i+1));
                caballos[i].setPriority(5);
                caballos[i].start();
            }
        }
        for(int i = 0; i < 10; i++){
            try {
                caballos[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
