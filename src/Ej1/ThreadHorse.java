package Ej1;

public class ThreadHorse implements Runnable{
    private int id;
    private int metros = 5000;

    public ThreadHorse(int id) {
        this.id = id;
    }

    @Override
    public void run() {
        do{
            for(int i = 0; i < 10; i++){
                metros-= 100;
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if(metros != 0){
                    System.out.println("Caballo " + id + ": " + metros + "m para finalizar");
                }else{
                    System.out.println("Caballo " + id + ": Ha Finalizado");
                }
            }
        }while (metros > 0);

    }
}
