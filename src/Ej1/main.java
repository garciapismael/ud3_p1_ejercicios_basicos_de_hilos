package Ej1;

public class main {
    public static void main(String[] args) {
        Thread[] caballos = new Thread[10];
        for(int i = 0; i < 10; i++){
            caballos[i] = new Thread(new ThreadHorse(i+1));
            caballos[i].start();
        }
        for(int i = 0; i < 10; i++){
            try {
                caballos[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
