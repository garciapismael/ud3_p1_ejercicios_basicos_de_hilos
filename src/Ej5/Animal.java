package Ej5;

public class Animal implements Runnable{
    private String nombre;
    private int metros = 3000;
    private int id;
    private int time = 10;

    public Animal(String nombre, int id) {
        this.nombre = nombre;
        this.id = id;
    }

    public Animal(String nombre, int id, int time) {
        this.nombre = nombre;
        this.id = id;
        this.time = time;
    }

    @Override
    public void run() {
        do{
            for(int i = 1; i <= 10; i++){
                metros-= 100;
                try {
                    Thread.sleep(time);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if(metros != 0){
                    System.out.println(nombre + " " + id +": " + metros + "m para finalizar");
                }else{
                    System.out.println(nombre + " " + id + ": Ha Finalizado");
                }
            }
        }while (metros > 0);
    }
}
