package Ej5;


public class Main {
    public static void main(String[] args) {
        //opcion que sleep
        sleep();
        //opcion con prioridades
        //priority();
    }

    public  static void sleep(){
        Thread[] animales = new Thread[9];
        for(int i = 1; i <= 4; i++){
            animales[i] = new Thread(new Animal("Tortugas", i, 30));
        }
        for(int i = 5; i <= 7; i++){
            animales[i] = new Thread(new Animal("Conejos", i,20));
        }
        animales[8] = new Thread(new Animal("Gepardo", 8));

        for(int i = 1; i <= 8; i++){
            animales[i].start();
        }
        for(int i = 1; i <= 8; i++){
            try {
                animales[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public  static void priority(){
        Thread[] animales = new Thread[9];
        for(int i = 1; i <= 4; i++){
            animales[i] = new Thread(new Animal("Tortugas", i));
            animales[i].setPriority(Thread.MIN_PRIORITY);
        }
        for(int i = 5; i <= 7; i++){
            animales[i] = new Thread(new Animal("Conejos", i));
            animales[i].setPriority(Thread.NORM_PRIORITY);
        }
        animales[8] = new Thread(new Animal("Gepardo", 8));
        animales[8].setPriority(Thread.MAX_PRIORITY);
        for(int i = 1; i <= 8; i++){
            animales[i].start();
        }

        for(int i = 1; i <= 8; i++){
            try {
                animales[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
