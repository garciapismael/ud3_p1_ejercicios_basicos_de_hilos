package Ej4;

public class ThreadHorse implements Runnable{
    private int id;
    private int metros = 5000;
    private boolean descalifica = false;

    public ThreadHorse(int id) {
        this.id = id;
    }

    @Override
    public void run() {
        try {
            do {
                for (int i = 1; i <= 10; i++) {
                    metros -= 100;

                    Thread.sleep(20);

                    if (metros != 0) {
                        System.out.println("Caballo " + id + ": " + metros + "m para finalizar");
                    } else {
                        System.out.println("Caballo " + id + ": Ha Finalizado");
                    }
                }
            } while (metros > 0);
        } catch (InterruptedException e) {
            System.out.println("Caballo dscalificado: " + id);
        }
    }
    public boolean isDescalifica(){
        descalifica = true;
        return descalifica;
    }
}
