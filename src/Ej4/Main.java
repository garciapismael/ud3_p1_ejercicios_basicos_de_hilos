package Ej4;

public class Main {
    public static void main(String[] args) {
        Thread[] caballos = new Thread[10];
        for (int i = 0; i < caballos.length; i++) {
            caballos[i] = new Thread(new ThreadHorse(i + 1));
        }
        Thread juez = new Thread(new JuezThread(caballos));
        for (int i = 0; i < caballos.length; i++) {
            caballos[i].start();
        }
        juez.start();
        for (int i = 0; i < caballos.length; i++) {
            try {
                caballos[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        try {
            juez.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
