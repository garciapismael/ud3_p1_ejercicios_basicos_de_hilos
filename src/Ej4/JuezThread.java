package Ej4;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class JuezThread implements Runnable {
    List<Thread> caballos = new ArrayList<>();
    int maxDesc = 5;
    int countDesc = 0;

    public JuezThread(Thread[] caballos) {
        for (int i = 0; i < caballos.length; i++) {
            this.caballos.add(caballos[i]);
        }

    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            if (countDesc < maxDesc) {
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                boolean descCaballo = ThreadLocalRandom.current().nextInt(0, 100) < 50;
                if (descCaballo) {
                    if (caballos.size() != 0) {
                        Thread caballoDesc = caballos.get(ThreadLocalRandom.current().nextInt(0, caballos.size()));
                        if (caballoDesc.isAlive()) {
                            caballoDesc.interrupt();
                            caballos.remove(caballoDesc);
                            countDesc++;
                        }
                        System.out.println("caballo descalificado");
                    }
                }
            }else break;
        }
    }
}
